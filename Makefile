all: minijazz processor simulator assembler clock

.PHONY: minijazz simulator processor clean assembler clock tar

assembler:
	$(MAKE) -C 'Compiler assembler' all

clock: assembler
	$(MAKE) -C 'Code horloge' all

minijazz:
	cd minijazz && ocamlbuild mjc.byte && cd ..

simulator:
	$(MAKE) -C simulator all

processor: minijazz
	$(MAKE) -C simulator/test processor

tar: clean
	rm -f horloge-buffiere-defossez-deng.tgz
	tar zcvf horloge-buffiere-defossez-deng.tgz *

clean:
	cd minijazz && rm -rf _build && rm -f mjc.byte & cd ..
	$(MAKE) -C simulator clean
	$(MAKE) -C simulator/test clean
	$(MAKE) -C 'Compiler assembler' clean
	$(MAKE) -C 'Code horloge' clean

