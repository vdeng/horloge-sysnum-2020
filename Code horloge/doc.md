# Comment fonctionne l'horloge ?

La conception de l'horloge avait pour objectif de fabriquer un code qui soit rapide à exécuter, car la partie la plus lente du projet est le microprocesseur. Il a fallu donc minimiser les divisions euclidiennes, pas question de recalculer à chaque fois les chiffres de l'horloge. C'est pourquoi l'horloge fonctionne avec une batterie de compteurs qui s'incrémentent au fur et à mesure. Par exemple, il y a un compteur du chiffre des unités des minutes qui se réinitialise à 10 pour avoir en permanence la valeur du chiffre correspondant. De même, pour calculer les années bissextiles, plutôt que recalculer le reste de la division euclidienne par 4, 100 ou 400 tous les ans, trois compteurs qui s'incrémentent tous les ans permettent de garder trace de cette congruence.

Ainsi, lors du passage à 2020, par exemple, le compteur du chiffre des unités des années est incrémenté de 9 à 10. Il est donc rebasculé à 0, puis le programme appelle en conséquence le compteur du chiffre des dizaines qui passe de 1 à 2. Il n'y a pas besoin de le réinitialiser. En parallèle, le compteur modulo 4 des années bissextiles passe de 3 à 4, donc est réinitialisé à 0, ce qui a pour effet de noter un 29 dans l'emplacement de nombre de jours de février. Le compteur modulo 100 et le compteur modulo 400 sont aussi incrémentés, mais comme ils valent tous deux 20, ils ne sont pas réinitialisés.

Tous ces compteurs sont stockés à des emplacements bien précis de la RAM, car il y en a trop pour utiliser seulement des registres.

# Comment s'organise le code ?

Le code de l'horloge est en quatre parties : 
 - l'initialisation (lignes 0 à 240) qui ne s'effectue qu'une fois. Elle est composée de sections qui s'exécutent les unes après les autres. Leur fonction est de récupérer les valeurs initiales de l'heure dans les emplacements dédiés de la RAM, d'en extraire les chiffres par division euclidienne, de stocker les chiffres de l'heure dans les emplacements réservés, et d'initialiser les segments de l'horloge à la vraie heure. C'est également cette portion du programme qui stocke dans la RAM le nombre de jours de chaque mois et qui initialise les compteurs de congruence pour calculer les années bissextiles. C'est la seule portion du programme qui effectue des divisions euclidiennes.
 - La boucle principale (lignes 240 à 250). Elle s'appelle elle-même tant que l'emplacement 1 de la RAM ne change pas de valeur, ou que l'instruction fast-forward est à 0.
 - Les incrémenteurs (250 à 620) : cette partie est composée de sections qui correspondent chacune à un chiffre de l'horloge. Lorsque la section correspondant à un chiffre est appelée, elle incrémente le compteur correspondant, change la valeur des segments et vérifie si d'autres sections sont à appeler (si le compteur du chiffre des unités des secondes est passé à zéro, le programme ne doit pas revenir dans la boucle principale, mais passer à l'incrémentation des dizaines de secondes.) Les incrémenteurs reviennent tôt ou tard à la boucle principale.
 - Les programmes secondaires (620 à 825) : deux sous-programmes sont codés à la fin et sont utilisés à divers moments. Ils ont une adresse de retour stockée dans r15. Le premier programme a pour fonction de modifier les segments d'un chiffre en recevant en argument l'emplacement RAM correspondant. L'autre programme est une division euclidienne récursive.

# Les emplacements en RAM :

0 : vaut 1 si le fast-forward est activé
1 : l'emplacement dont la valeur est changée toutes les secondes
3 : emplacement initial des années
4 : emplacement initial des mois
5 : emplacement initial des jours
6 : emplacement initial des heures
7 : emplacement initial des minutes
8 : emplacement initial des secondes

Viennent ensuite les emplacements dédiés aux segments. Chaque chiffre a 8 emplacements dédiés : les sept premiers correspondent aux valeurs des segments (1 si allumé, 0 sinon), et le 8e est le compteur contenant le valeur du chiffre :

256-263 : chiffre des dizaines du jour
272-279 : chiffre des unités du jour
288-295 : chiffre des dizaines du mois
304-311 : chiffre des unités du mois
320-327 : chiffre des millénaires
336-343 : chiffre des siècles
352-359 : chiffre des décennies
368-375 : chiffre des unités de l'année
384-391 : chiffre des dizaines de l'heure
400-407 : chiffre des unités de l'heure
416-423 : chiffre des dizaines de minutes
432-439 : chiffre des unités de minutes
448-455 : chiffre des dizaines de secondes
464-471 : chiffre des unités de secondes

Restent quelques emplacements dédiés à des compteurs spéciaux (la valeur de réinitialisation est indiquée quand elle est constante)
481-492 : nombre de jours de chaque mois
280 : numéro du jour actuel ; 328 : numéro du mois actuel (max 12)
281 : nombre de jours maximal du mois actuel (pas un compteur)
408 : numéro de l'heure (max 24)
377 : année actuelle modulo 4 (max 4)
378 : année actuelle modulo 100 (max 100)
379 : année actuelle modulo 400 (max 400)

# Les étiquettes :

Un défaut de l'assembleur, reconnaissons-le, est d'accepter que des nombres pour les étiquettes.
Voici donc la signification des principales étiquettes. Les autres étiquettes correspondent généralement à des sous-portions de programme étiquetés. Par exemple, l'étiquette $1501 est correspond à une commande IF à l'étiquette $15.

$300 à $400 : étiquettes des initialisations. Ces étiquettes sont souvent appelées pour effectuer des IF pendant l'initialisation.
$10 : étiquette la plus appelée : c'est la boucle principale.
$11 : Incrémentateur des secondes
$12 : Incrémentateur des dizaines de secondes
etc. jusqu'à $25 qui est l'incrémentateur des millénaires.
(Notons que l'étiquette $21 correspond à l'incrémentateur des compteurs modulo 4, 100 et 400 des années (pour les années bissextiles))
$100 : programme d'écriture des chiffres
$110 à $119 : écriture du chiffre 0, 1, ... ou 9. (113 correspond à l'écriture de 3)
$2 programme de division euclidienne.


