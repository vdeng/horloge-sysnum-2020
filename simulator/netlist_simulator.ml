open Netlist_ast

let buffer_len = 1024

let quiet = ref false (* Ne pas afficher les sorties *)
let quiet_input = ref false (* Ne pas afficher les messages d'entrée *)
let input_ram = ref false (* Entrer les valeurs initiales de la/des RAM *)
let number_steps = ref (-1) (* Nombre de cycles à simuler, -1 si infini *)
let rom_location = ref ""
let fast_forward = ref false (* Mode rapide de l'horloge *)
(*let slow_print = ref false (* Ne mettre à jour l'affichage que lorsque l'horloge se met à jour *) *)
let clock_display = ref false (* Mode horloge : afficher l'horloge en ASCII art et ne la mettre à jour que si nécessaire *)
let clock_input = ref false (* Activation des adresses spéciales de RAM *)
let debug_perf = ref false (* Afficher des durées d'exécution d'un cycle *)
let ifile = ref ""
let ram_size = ref (-1)
let rom_size = ref (-1)
let input_rom_bytes = ref (Bytes.create 0)
let is_ff_addr = 0x00000000
let clock_addr = 0x00000001
let timestamp_addr = 0x00000002
let year_addr = 0x00000003
let month_addr = 0x00000004
let day_addr = 0x00000005
let hour_addr = 0x00000006
let min_addr = 0x00000007
let sec_addr = 0x00000008
let disp_addr_lo = 0x00000100
let disp_addr_hi = 0x000001d6
(* Affichage : 0xf00001(chiffre)(seg) *)

let display = Array.make_matrix 14 7 true

let start_ts = Float.to_int (Unix.time ())
let start_time = Unix.localtime (Unix.time ())
let start_components = [|
  start_time.tm_year+1900;
  start_time.tm_mon+1;
  start_time.tm_mday;
  start_time.tm_hour;
  start_time.tm_min;
  start_time.tm_sec
  |]
(*
      else if idx = year_addr then Some (start_time.tm_year + 1900)
      else if idx = month_addr then Some (start_time.tm_mon + 1)
      else if idx = day_addr then Some (start_time.tm_mday)
      else if idx = hour_addr then Some (start_time.tm_hour)
      else if idx = min_addr then Some start_time.tm_min
      else if idx = sec_addr then Some start_time.tm_sec*)
let updated_display = ref true

exception InvalidInput

let update_display addr bitarr =
  let word_sz = Array.length bitarr in
  let idigit = (addr lsr 4) land 0xf in
  let iseg = addr land 0xf in
  if idigit >= 0 && idigit <= 13 && iseg >= 0 && iseg <= 6 then
    if display.(idigit).(iseg) <> bitarr.(word_sz-1) then (
      display.(idigit).(iseg) <- bitarr.(word_sz-1);
      updated_display := true
    )

let ascii_digit i =
  [|[| ' ', None;        '_', Some (i, 0); ' ', None |];
    [| '|', Some (i, 1); '_', Some (i, 2); '|', Some (i, 3)|];
    [| '|', Some (i, 4); '_', Some (i, 5); '|', Some (i, 6)|]|]

let ascii_slash () =
  [|[| ' ', None; ' ', None|];
    [| ' ', None; '/', None|];
    [| '/', None; ' ', None|]|]

let ascii_space () =
  [|[| ' ', None|];
    [| ' ', None|];
    [| ' ', None|]|]

let ascii_colon () =
  [|[| ' ', None|];
    [| '.', None|];
    [| '.', None|]|]

let ascii_table = 
  let components = [ascii_digit 0; ascii_digit 1; ascii_space (); ascii_slash ();  ascii_space();
    ascii_digit 2; ascii_digit 3; ascii_space(); ascii_slash(); ascii_space();
    ascii_digit 4; ascii_digit 5; ascii_digit 6; ascii_digit 7; ascii_space(); ascii_space (); ascii_space();
    ascii_digit 8; ascii_digit 9; ascii_space(); ascii_colon(); ascii_space();
    ascii_digit 10; ascii_digit 11; ascii_space(); ascii_colon(); ascii_space();
    ascii_digit 12; ascii_digit 13
    ] in
  let matrix = Array.make 3 (Array.make 0 (' ', None)) in
  for i=0 to 2 do
    let rec concat_line = function
    | [] -> []
    | x::q -> x.(i)::concat_line q
    in matrix.(i) <- Array.concat (concat_line components)
  done;
  matrix

let clear_linux_term () =
  Format.printf "\027[2J%!"

let display_ascii () =
  for lin = 0 to (Array.length ascii_table)-1 do
    let lin_desc = ascii_table.(lin) in
    let disp (car, opt) = match opt with
    | None -> print_char car
    | Some (idig, iseg) ->
      let should = display.(idig).(iseg) in
      print_char (if should then car else ' ')
    in Array.iter disp lin_desc;
    print_newline ()
  done;
  print_newline ()

(* Convertit un tableau de booléens en entier, interprétation little-endian *)
let bitarr_to_int arr =
  let ret = ref 0 in
  for i = 0 to (Array.length arr)-1 do
    ret := (!ret lsl 1) lor (if arr.(i) then 1 else 0)
  done;
  !ret

let int_to_bitarr word_sz k =
  let ret = Array.make word_sz false in
  for i=0 to word_sz-1 do
    ret.(i) <- (k lsr (word_sz-1-i)) mod 2 = 1
  done;
  ret

(* Convertit l'entrée binaire (00101010) ou décimale (d42) en tableau de booléens, interprétation little-endian.
   La longueur souhaitée du tableau est passée dans le paramètre exp_len. *)
let str_to_bitarr str exp_len =
  let n = String.length str in
  if n = 0 then
    raise InvalidInput;
  if str.[0] <> 'd' then (
    if n <> exp_len then raise InvalidInput;
    let arr = Array.make exp_len false in
    for i=0 to exp_len-1 do
      if str.[i] = '0' then
        arr.(i) <- false
      else if str.[i] = '1' then
        arr.(i) <- true
      else
        raise InvalidInput
    done;
    arr
  )
  else (
    try (
      let arr = Array.make exp_len false in
      let nb = int_of_string (String.sub str 1 (n - 1)) in
      for i=0 to exp_len-1 do
        arr.(exp_len-1-i) <- ((nb lsr i) land 1) = 1
      done;
      arr
    ) with Failure _ -> raise InvalidInput
  )

let bitarr_to_str arr =
  String.init (Array.length arr) (fun k -> if arr.(k) then '1' else '0')

let is_ram = function | NEram _ -> true | _ -> false

let encountered_rom = ref false
let encountered_ram = ref false

(* Initialise une RAM ou ROM, avec entrée de la valeur par l'utilisateur si besoin. *)
let rec create_ramrom ram rom var_arr ((id: nident), exp) = match exp with
| NEram (addr_sz, word_sz, _, _, _, _) | NErom (addr_sz, word_sz, _) ->
  let sz =
    if is_ram exp && not !encountered_ram && !ram_size <> -1 then
      (encountered_ram := true; !ram_size)
    else if not (is_ram exp) && not !encountered_rom && (!rom_location <> "" || !rom_size <> -1) then
      (if !rom_location <> "" then
        let num_bytes = Bytes.length !input_rom_bytes in
        let num_bits = 8*num_bytes in
        num_bits/word_sz
      else
        !rom_size)
    else
      1 lsl addr_sz
    in
  Format.eprintf "Creating a zeroed RAM/ROM of %d words of %d bits...\n%!" sz word_sz;
  let curr_ram = Array.make_matrix sz word_sz false in (* liaison résolue ci-dessous *)
  (* Entrée par l'utilisateur *)
  Format.eprintf "Zeroing done!\n%!";
  if not (is_ram exp) && not !encountered_rom && !rom_location <> "" then begin
    encountered_rom := true;
    Format.eprintf "Reading %d words from ROM file...\n%!" sz;
    for i=0 to sz-1 do
      let start_bit = i*word_sz in
      for ibit=0 to word_sz-1 do
        let bit_pos = start_bit + ibit in
        let byte_pos = bit_pos/8 in
        let offs = bit_pos mod 8 in
        let roffs = 7 - offs in
        let byte = Bytes.get_uint8 !input_rom_bytes byte_pos in
        curr_ram.(i).(ibit) <- ((byte lsr roffs) land 1) = 1;
      done
    done;
    Format.eprintf "Done reading ROM.\n%!"
  end
  else if not (is_ram exp) || !input_ram then begin
    if not !quiet_input then
      Printf.printf "Please input RAM/ROM %s's initial values; %d words, word size %d.\n%!" (fst var_arr.(id)) sz word_sz;
    for i=0 to sz-1 do
      let rec input () = 
        if not !quiet_input then
          Printf.printf "#%d: %!" i;
        Scanf.scanf " %s" (function str -> 
          try (
            let arr = str_to_bitarr str word_sz in
            curr_ram.(i) <- arr
          ) with InvalidInput -> (
            Printf.printf "Invalid input.\n%!";
            input()
          )
        )
      in input ()
    done
  end
  (* Zéros *)
  (*else (
    Format.eprintf "Zeroing a ROM/RAM of %d words of %d bits...\n%!" sz word_sz;
    for i=0 to sz-1 do
      curr_ram.(i) <- Array.make word_sz false
    done;
    Format.eprintf "Finished zeroing RAM/ROM.\n%!")*);
  begin
    match exp with
    | NEram _ -> Hashtbl.replace ram id curr_ram
    | NErom _ -> Hashtbl.replace rom id curr_ram
    | _ -> failwith "Unreachable error in create_ramrom"
  end
| _ -> ()

(* Trouve tous les registres dans un programme afin de recopier leurs valeurs en début de cycle *)
let rec find_regs = function
| [] -> []
| (_, NEreg id)::q -> id::find_regs q
| _::q -> find_regs q

(* Détermine si une valeur peut être interprétée comme un bit : c'est le cas pour les VBit et les VBitArray de taille 1. *)
let is_bit arr = Array.length arr = 1

(* Ne pas modifier les valeurs renvoyées *)
let to_array = function
| VBit k -> [|k|]
| VBitArray arr -> arr

(* À gérer plus tard : affichage, horloge *)
let read_romram is_ram romram word_sz idx =
  if not is_ram then begin
    if idx < Array.length romram then
      romram.(idx)
    else
      int_to_bitarr word_sz 0
  end
  else begin
    let nb =
      if !clock_input then begin
        if idx = clock_addr then Some ((Float.to_int (Unix.time ())) mod 2)
        else if idx = is_ff_addr then Some ((if !fast_forward then 1 else 0))
        else if idx = timestamp_addr then Some start_ts
        (*else if idx = year_addr then Some (start_time.tm_year + 1900)
        else if idx = month_addr then Some (start_time.tm_mon + 1)
        else if idx = day_addr then Some (start_time.tm_mday)
        else if idx = hour_addr then Some (start_time.tm_hour)
        else if idx = min_addr then Some start_time.tm_min
        else if idx = sec_addr then Some start_time.tm_sec*)
        else if idx >= year_addr && idx <= sec_addr then Some start_components.(idx - year_addr)
        else None
      end else None
    in match nb with
    | Some k -> int_to_bitarr word_sz k
    | None ->
      if idx < Array.length romram then
        romram.(idx)
      else
        int_to_bitarr word_sz 0
  end


let write_ram romram word_sz idx value =
  if idx < Array.length romram then
    romram.(idx) <- value;
  if idx >= disp_addr_lo && idx <= disp_addr_hi then
    update_display idx value

let simulator (program: nprog) number_steps =
  if not !quiet_input then begin
    Printf.printf "When the program requests a bit array, you may input it either in\n";
    Printf.printf "binary form (e.g. 00101010), or decimal form with a trailing 'd'\n";
    Printf.printf "(e.g. d42) with little-endian convention.\n";
    Printf.printf "Any bit beyond the array's size will be discarded. Decimal values\n";
    Printf.printf "outside [-2^63, 2^63-1] are unsupported.\n\n%!"
  end;

  (* Initialisation des variables *)
  Format.eprintf "Initializing variables...\n%!";
  let nb_vars = Array.length program.np_vars in
  let vars = Array.make nb_vars [||] in
  let ram = Hashtbl.create 1 and rom = Hashtbl.create 1 in
  let init_vars ind (_,typ) = match typ with
    | TBit -> vars.(ind) <- (Array.make 1 false)
    | TBitArray k -> vars.(ind) <- (Array.make k false)
  in Array.iteri init_vars program.np_vars;
  Format.eprintf "Initializing RAM/ROMs...\n%!";
  List.iter (create_ramrom ram rom program.np_vars) program.np_eqs;
  let regs = find_regs program.np_eqs in
  let nb_regs = List.length regs in
  Format.eprintf "Done initializing.\n%!";

  let step = ref 0 in
  while (number_steps < 0 || !step < number_steps) do

    let start_read_time = Sys.time() in
    (* Lecture des entrées *)
    let rec read_var id = 
      let oname, typ = program.np_vars.(id) in
      match typ with
      | TBit -> 
        if not !quiet_input then
          Printf.printf "%s (bit): %!" oname; 
        Scanf.scanf " %d" (function 
          | 0 -> vars.(id) <- [|false|]
          | 1 -> vars.(id) <- [|true|]
          | _ -> Printf.printf "Invalid input.\n";read_var id)
      | TBitArray size ->
        if not !quiet_input then
          Printf.printf "%s (%d bits): %!" oname size;
        Scanf.scanf " %s" (function str ->
          let arr = vars.(id) in
          let arrlen = Array.length arr in
          try (
            let read_arr = str_to_bitarr str arrlen in
            for ibit=0 to arrlen-1 do
              arr.(ibit) <- read_arr.(ibit)
            done
          ) with InvalidInput -> (
            Printf.printf "Invalid input.\n"; read_var id
          )
        )
    in List.iter read_var program.np_inputs;




    (* Simulation *)
    (* Copie des valeurs des registres *)
    let start_copy_reg_time = Sys.time() in
    let old_vars = Hashtbl.create nb_regs in
    let copy_var id = 
        let arr = vars.(id) in
        Hashtbl.replace old_vars id (Array.copy arr)
    in List.iter copy_var regs;



    let start_eval_time = Sys.time() in
    (* Risque de liaison pour les tableaux : résolu lors de l'affectation des variables.
    Le tableau renvoyé ne doit en aucun cas être modifié. *)
    let eval_arg = function
      | NAvar id -> vars.(id)
      | NAconst v -> v
    in
    (* Lectures de RAM effectuées en fin de cycle *)
    let ram_writes = ref [] in

    (* id_romram est l'identifiant de la variable de gauche, et n'est utilisée que pour du débogage et les écritures en RAM *)
    let eval_exp id_romram = function 
      | NEarg arg -> eval_arg arg (* Un tableau intervenant ici n'est plus modifié jusqu'en fin de cycle : pas de recopie *)

      | NEreg id -> Hashtbl.find old_vars id

      | NEnot arg -> 
        let v = eval_arg arg in
        (*if not (is_bit v) then
          failwith ("Enot of non-singleton array is undefined (trying to replace "^(fst program.np_vars.(id_romram))^")");*)
        [| not v.(0) |]

      | NEbinop (bin, arg1, arg2) -> let val1, val2 = eval_arg arg1, eval_arg arg2 in
        (*if not (is_bit val1) || not (is_bit val2) then
          failwith ("Binop with non-singleton arrays is undefined (trying to replace "^(fst program.np_vars.(id_romram))^")");*)
        let b1 = val1.(0) and b2 = val2.(0) in
        begin match bin with
        | Or -> [|b1 || b2|]
        | And -> [|b1 && b2|]
        | Xor -> [|(b1 && not b2) || (b2 && not b1)|]
        | Nand -> [|not (b1 && b2)|]
        end

      | NEmux (sel, arg0, arg1) -> (* Interprétation : MUX a b c = c si a=1, b si a=0 *)
        let vsel = eval_arg sel in
        (*if not (is_bit vsel) then failwith ("Emux with array as first argument is undefined (trying to replace "^(fst program.np_vars.(id_romram))^")");*)
        if vsel.(0) then eval_arg arg1 else eval_arg arg0 (* Même remarque pour les tableaux *)

      | NErom (addr_sz, word_sz, arg_read_ad) ->
        let arr = eval_arg arg_read_ad in
        (*if Array.length arr = addr_sz then *)
          (*(Hashtbl.find rom id_romram).(bitarr_to_int arr)*)
          read_romram false (Hashtbl.find rom id_romram) word_sz (bitarr_to_int arr)
        (*else
          failwith ("Bad read address for ROM (trying to replace "^(fst program.np_vars.(id_romram))^")")*)

      | NEram (addr_sz, word_sz, arg_read_ad, arg_write_en, arg_write_ad, arg_data) ->
        (* Toutes les écritures se font à la fin. En particulier les adresses et données à écrire sont
        déterminées à la fin du cycle. *)
        let ram_arr = Hashtbl.find ram id_romram in
        let read_ad = eval_arg arg_read_ad in
        (*if Array.length read_ad <> addr_sz then
          failwith ("Bad ram access: wrong size or type of read address ("^(fst program.np_vars.(id_romram))^")");*)
        let va = read_romram true ram_arr word_sz (bitarr_to_int read_ad) in
        ram_writes := (id_romram, arg_write_en, arg_write_ad, arg_data, addr_sz, word_sz)::!ram_writes;
        va

      | NEconcat (arg1, arg2) ->
        let arr1 = eval_arg arg1 and arr2 = eval_arg arg2 in
        Array.append arr1 arr2

      | NEslice (i1,i2,arg) ->
        Array.sub (eval_arg arg) i1 (i2-i1+1)

      | NEselect (i1,arg) ->
        let arr = eval_arg arg in
        [|arr.(i1)|]
    in
    (* Evalue l'expression de droite et stocke sa valeur dans la variable de gauche. Tout tableau est copié. *)
    let eval_eqn (id, exp) =
      let (*ty = snd program.np_vars.(id)
      and*) va = eval_exp id exp in
      (* On pourrait ne pas vérifier les longueurs de tableaux pour aller plus vite *)
      (*let exp_len = match ty with
      | TBit -> 1
      | TBitArray k -> k
      in
      if Array.length va <> exp_len then
        failwith (Format.asprintf "Expected length %d for variable %s, got %d instead" exp_len (fst program.np_vars.(id)) (Array.length va));*)
      vars.(id) <- (Array.copy va) (* pour éviter les liaisons *)
    in List.iter eval_eqn program.np_eqs;



    (* Ecritures en RAM *)
    let start_ramwrite_time = Sys.time() in
    let write_ram (id_romram, arg_write_en, arg_write_ad, arg_data, addr_sz, word_sz) =
      try
        if (eval_arg arg_write_en).(0) then begin
          let curr_ram = Hashtbl.find ram id_romram in
          let warr = eval_arg arg_write_ad in
          (*if Array.length warr <> addr_sz then
            failwith ("Bad ram write: wrong size or type of write address("^(fst program.np_vars.(id_romram))^")");*)
          let darr = eval_arg arg_data in
          (*if Array.length darr <> word_sz then
            failwith ("Bad ram write: wrong size or type of data ("^(fst program.np_vars.(id_romram))^")");*)
          let write_ad = bitarr_to_int warr in
          if not !quiet && (!step mod 100 = 0 || not !clock_display) then
            Format.eprintf "Writing %s (%d) to RAM %d at address %s (%d)\n%!" (bitarr_to_str darr) (bitarr_to_int darr) id_romram (bitarr_to_str warr) write_ad;
          write_ram curr_ram word_sz write_ad (Array.copy darr)
        end
      with Failure _ -> failwith ("Bad ram write: got a VBitArray as write_enable when processing RAM "^(fst program.np_vars.(id_romram)))
    in List.iter write_ram !ram_writes;



    let end_time = Sys.time() in
    (* Affichage des sorties *)
    if not !quiet && (!step mod 100 = 0 || not !clock_display || !updated_display) then (
      updated_display := false;
      if !clock_display then
        clear_linux_term()
      else
        Printf.printf "\n\n";
      Printf.printf "Step %d:\n" !step;
      if !debug_perf then begin
        Printf.printf "Input phase: %fs\n" (start_copy_reg_time -. start_read_time);
        Printf.printf "Register copy phase (there are %d): %fs\n" nb_regs (start_eval_time -. start_copy_reg_time);
        Printf.printf "Evaluation phase: %fs\n" (start_ramwrite_time -. start_eval_time);
        Printf.printf "Ram write phase: %fs\n" (end_time -. start_ramwrite_time)
      end;
      Printf.printf "Outputs:\n";
      let show_var id = 
        let oname, typ = program.np_vars.(id) in
        match typ with
        | TBit -> Printf.printf "%s: %d\n" oname (if vars.(id).(0) then 1 else 0)
        | TBitArray _ -> 
          let arr = vars.(id) in
          Printf.printf "%s: %d %s\n" oname (bitarr_to_int arr) (bitarr_to_str arr);
      in List.iter show_var program.np_outputs;
      if !clock_display then begin
        Printf.printf "Clock state:\n";
        display_ascii();
      end;
      Printf.printf "\n\n%!";
    );
    incr step
  done

let compile filename =
  try
    Format.eprintf "Reading netlist...\n%!";
    let p = Netlist.read_file filename in
    begin try
        Format.eprintf "Simplifying netlist...\n%!";
        let p = Simplifier.remove_trivial_assigns (Simplifier.remove_trivial_slices (Scheduler.schedule p)) in
        Format.eprintf "-> Removed %d trivial slices and %d trivial assignments\n" !Simplifier.removed_slices !Simplifier.removed_eqs;
        if !debug_perf then
          Simplifier.print_statistics p;
        Format.eprintf "Starting simulation...\n\n\n%!";
        simulator p !number_steps
      with
        | Scheduler.Combinational_cycle ->
            (Format.eprintf "ERROR: The netlist has a combinatory cycle.@."; exit 1)
    end;
  with
    | Netlist.Parse_error s -> Format.eprintf "ERROR: Failed to parse the netlist: %s@." s; exit 2

let set_ifile s =
  if !ifile = "" then
    ifile := s
  else begin
    Format.eprintf "Got several input files, expected only one\n";
    exit 1
  end

let read_file () =
  if !rom_location <> "" then begin
    Format.eprintf "Reading ROM from %s...\n%!" !rom_location;
    let ic = open_in_bin !rom_location in
    let buf = Bytes.create buffer_len in
    let read_len = ref (-1) in
    let buf_list = ref [] in
    while (read_len := input ic buf 0 buffer_len; !read_len) <> 0 do
      buf_list := (Bytes.sub buf 0 !read_len)::!buf_list
    done;
    close_in ic;
    input_rom_bytes := Bytes.concat (Bytes.create 0) (List.rev !buf_list);
    Format.eprintf "Finished reading ROM.\n%!";
  end

let parse_date s =
  try
    Scanf.sscanf s "%d-%d-%dT%d:%d:%d" (fun year month day hour min sec ->
      start_components.(0) <- year;
      start_components.(1) <- month;
      start_components.(2) <- day;
      start_components.(3) <- hour;
      start_components.(4) <- min;
      start_components.(5) <- sec)
  with Scanf.Scan_failure _ -> (
    Format.eprintf "ERROR: Couldn't parse the given date. This date should be in format (year)-(month)-(day)T(hour):(min):(sec).\n%!";
    exit 1
  )

let main () =
  Arg.parse
    ["-c", Arg.Unit (fun () -> clock_display := true; clock_input := true), "Enable clock mode: displays the clock and enables clock inputs. Shorthand for --clock-display --clock-input";
     "--clock-display", Arg.Set clock_display, "Display the clock in ASCII art and only updates display when the clock changes";
     "--clock-input", Arg.Set clock_input, "Enable special RAM addresses for clock input";
     "--debug-perf", Arg.Set debug_perf, "Show running time statistics";
     "-ff", Arg.Set fast_forward, "Run clock in fast-forward mode";
     "--idate", Arg.String parse_date, "Set the initial date to the given date instead of the current system time. The initial date must be in format (year)-(month)-(day)T(hour):(min):(sec).";
     "-n", Arg.Set_int number_steps, "Number of steps to simulate (default is infinite)";
     "-q", Arg.Set quiet, "Disable all output printing";
     "-Q", Arg.Set quiet_input, "Disable all input messages (useful for batch testing)";
     "-r", Arg.Set input_ram, "Input initial RAM values";
     "--ram-size", Arg.Set_int ram_size, "Physical RAM size in number of words. Do not use this if there are multiple RAMs in the netlist. Defaults to 2^(addr_sz)";
     "-R", Arg.Set_string rom_location, "File containing the only ROM's values, assuming there is only one ROM in the netlist";
     "--rom-size", Arg.Set_int rom_size, "Physical ROM size in number of words, ignored if an input file is specified with -R. Do not use this if there are multiple ROMs in the netlist. Defaults to 2^(addr_sz)"]
    set_ifile
    "Usage: ./netlist_simulator.native [options] <file.net>. Type ./netlist_simulator.byte -help for help.";
  if !ifile = "" then begin
    Format.eprintf "No input file. Type './netlist_simulator.native -help' for help.\n";
    exit 1
  end;
  read_file();
  compile !ifile
;;

main ()
