open Graph
open Netlist_ast

let removed_slices = ref 0

let removed_eqs = ref 0

let remove_trivial_slices p =
  let process_nexp eq = match eq with
  | NEslice (i1, i2, arg) -> begin
    match arg with
    | NAvar id -> begin
      match snd p.np_vars.(id) with
      | TBit ->
        assert (i1 = 0 && i2 = 0); incr removed_slices;
        NEarg arg
      | TBitArray width when i1=0 && i2=width-1 ->
        incr removed_slices;
        NEarg arg
      | _ -> eq
      end
    | NAconst ar when i1 = 0 && i2 = Array.length ar - 1 ->
      incr removed_slices; NEarg arg
    | _ -> eq
    end
  | NEselect (i, arg) -> begin
    match arg with
    | NAvar id -> begin
      match snd p.np_vars.(id) with
      | TBit -> assert (i=0); incr removed_slices; NEarg arg
      | TBitArray width when width = 1 -> assert (i=0); incr removed_slices; NEarg arg
      | _ -> eq
      end
    | NAconst ar when Array.length ar = 1 ->
      assert (i = 0); incr removed_slices; NEarg arg
    | _ -> eq
    end
  | _ -> eq
  in
  let process_neqn (nid, neq) = (nid, process_nexp neq) in
  {
    np_eqs = List.map process_neqn p.np_eqs;
    np_inputs = p.np_inputs;
    np_outputs = p.np_outputs;
    np_vars = p.np_vars
  }

let remove_trivial_assigns p =
  let nb_vars = Array.length p.np_vars in
  let parent = Array.make nb_vars (-1) in
  let fill_with_eq (nid, nexp) = match nexp with
  | NEarg arg -> begin
    match arg with
    | NAvar p_id -> parent.(nid) <- p_id
    | NAconst _ -> ()
    end
  | _ -> ()
  in List.iter fill_with_eq p.np_eqs;
  let rec find_parent id =
    if parent.(id) = -1 then
      id
    else begin
      parent.(id) <- find_parent parent.(id);
      parent.(id)
    end
  in
  let is_output = Array.make nb_vars false in
  List.iter (fun x -> is_output.(x) <- true) p.np_outputs;
  let replace_arg = function
  | NAvar id -> NAvar (find_parent id)
  | a -> a
  in
  let rec replace_eqs = function
  | [] -> []
  | ((nid, eq) as old)::q ->
    match eq with
    | NEarg arg -> begin
      match arg with
      | NAvar id ->
        if is_output.(nid) then
          (nid, NEarg (replace_arg arg))::replace_eqs q
        else begin
          incr removed_eqs; replace_eqs q
        end
      | _ -> old::replace_eqs q
      end
    | NEreg rid -> (nid, NEreg (find_parent rid))::replace_eqs q
    | NEnot a -> (nid, NEnot (replace_arg a))::replace_eqs q
    | NEbinop (op, a1, a2) -> (nid, NEbinop (op, replace_arg a1, replace_arg a2))::replace_eqs q
    | NEmux (a1, a2, a3) -> (nid, NEmux (replace_arg a1, replace_arg a2, replace_arg a3))::replace_eqs q
    | NErom (asz, wsz, rad) -> (nid, NErom (asz, wsz, replace_arg rad))::replace_eqs q
    | NEram (asz, wsz, rad, wen, wad, wd) -> (nid, NEram (asz, wsz, replace_arg rad, replace_arg wen, replace_arg wad, replace_arg wd))::replace_eqs q
    | NEconcat (a1, a2) -> (nid, NEconcat (replace_arg a1, replace_arg a2))::replace_eqs q
    | NEslice (i1, i2, a) -> (nid, NEslice (i1, i2, replace_arg a))::replace_eqs q
    | NEselect (i, a) -> (nid, NEselect (i, replace_arg a))::replace_eqs q
  in
  {
    np_eqs = replace_eqs p.np_eqs;
    np_inputs = p.np_inputs;
    np_outputs = p.np_outputs;
    np_vars = p.np_vars
  }

let print_statistics p =
  let num_instr = List.length p.np_eqs in
  let num_reg = ref 0 in
  let num_assign = ref 0 in
  let num_not = ref 0 in
  let num_binop = ref 0 in
  let num_mux = ref 0 in
  let num_rom = ref 0 in
  let num_ram = ref 0 in
  let num_concat = ref 0 in
  let num_slice = ref 0 in
  let num_select = ref 0 in
  let process_eq (_, ex) = match ex with
  | NEarg _ -> incr num_assign
  | NEreg _ -> incr num_reg
  | NEnot _ -> incr num_not 
  | NEbinop _ -> incr num_binop
  | NEmux _ -> incr num_mux
  | NErom _ -> incr num_rom
  | NEram _ -> incr num_ram
  | NEconcat _ -> incr num_concat
  | NEslice _ -> incr num_slice
  | NEselect _ -> incr num_select
in
  List.iter process_eq p.np_eqs;
  Format.eprintf "Number of instructions: %d, of which\n%d REG\n%d assignments\n%d NOT\n%d binops\n%d MUX\n%d ROM\n%d RAM\n%d CONCAT\n%d SLICE\n%d SELECT\n%!" num_instr !num_reg !num_assign !num_not !num_binop !num_mux !num_rom !num_ram !num_concat !num_slice !num_select