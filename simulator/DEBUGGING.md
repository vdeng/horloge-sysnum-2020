# Options de débogage

Le simulateur supporte plusieurs options permettant de définir son comportement en termes d'affichage. 
Avant le démarrage de la simulation, il affiche les simplifications effectuées. 
Par défaut, à chaque étape, il affiche les valeurs des sorties et les écritures en RAM, en binaire et en décimal. Pour l'instant, une écriture en RAM se rapporte au cycle affiché juste après et est affichée sur l'erreur standard (contrairement au reste des affichages, qui se font sur la sortie standard).

- `-c` : mode horloge. L'affichage est mis à jour tous les 100 cycles et lorsqu'un des segments de l'horloge est modifié.
- `--debug-perf` : affiche des statistiques de temps d'exécution sur un cycle
- `-q` : n'affiche rien

Exemples de débogage :
- Afficher uniquement les écritures en RAM : `../netlist_simulator.native --ram-size 100000 -R ../../Code\ horloge/clock.bin proc_simple.net 2>&1|grep RAM`
- Afficher uniquement les écritures dans la case 455 de la RAM : `../netlist_simulator.native --ram-size 100000 -R ../../Code\ horloge/clock.bin proc_simple.net 2>&1|grep RAM.*455`
