open Netlist_ast
open Graph

exception Combinational_cycle

let rec sans_doublon = function
  | [] -> []
  | x::q -> let q2 = sans_doublon q in
      if List.mem x q2 then q2 else x::q2

let read_exp eq =
  let _, ex = eq in
  let read_arg = function
    | Avar id2 -> [id2]
    | _ -> []
  in sans_doublon (match ex with
    | Earg ar -> read_arg ar
    | Ereg id2 -> [id2]
    | Enot ar -> read_arg ar
    | Ebinop (_,ar1,ar2) -> read_arg ar1@read_arg ar2
    | Emux (ar1,ar2,ar3) -> read_arg ar1@read_arg ar2@read_arg ar3
    | Erom (_,_,ar) -> read_arg ar
    | Eram (_,_,ar1,_,_,_) -> read_arg ar1(*@read_arg ar3@read_arg ar4*) (* Toutes les écritures se font à la fin : pas besoin de demander les deux derniers arguments*)
    | Econcat (ar1,ar2) -> read_arg ar1@read_arg ar2
    | Eslice (_,_,ar) -> read_arg ar
    | Eselect (_,ar) -> read_arg ar)

let schedule p = (
  (*Printf.eprintf "Beginning scheduling...\n%!";*)
  let eqs = p.p_eqs in
  let g = mk_graph () in
  let vars = List.map fst (Env.bindings p.p_vars) in
  List.iter (add_node g) vars;
  let rec add_edges = function
    | [] -> ()
    | (id, Ereg _)::q -> add_edges q
    | (id,ex)::q -> let inputs = read_exp (id,ex) in List.iter (function x -> add_edge g x id) inputs;add_edges q
  in add_edges eqs;
  try (
    let toposort = topological g in
    let eqn_map = Env.of_list eqs in
    let aux (map, next) lab = Env.add lab next map, (next+1) in
    let (var_map, nb_var) = List.fold_left aux (Env.empty, 0) toposort in
    let var_arr = Array.make nb_var ("", TBit) in
    let aux2 lab =
      let i = Env.find lab var_map in
      var_arr.(i) <- (lab, Env.find lab p.p_vars)
    in
    List.iter aux2 toposort;
    let rec explore = function
      | [] -> []
      | var::q -> 
        if Env.mem var eqn_map then begin
          to_neqn var_map (var, Env.find var eqn_map)::explore q
        end else
          explore q (* Cas d'une variable nulle *)
    in {
      np_eqs = explore toposort;
      np_inputs = List.map (fun l -> Env.find l var_map) p.p_inputs;
      np_outputs = List.map (fun l -> Env.find l var_map) p.p_outputs;
      np_vars = var_arr
    }
  ) with Cycle -> raise Combinational_cycle)

let schedule_legacy p = (
  let eqs = p.p_eqs in
  let g = mk_graph () in
  let vars = List.map fst (Env.bindings p.p_vars) in
  List.iter (add_node g) vars;
  let rec add_edges = function
    | [] -> ()
    | (id, Ereg _)::q (*| (id, Eram _)::q*) -> add_edges q
    | (id,ex)::q -> let inputs = read_exp (id,ex) in List.iter (function x -> add_edge g x id) inputs;add_edges q
  in add_edges eqs;
  try (
    let toposort = topological g in
    let eqn_map = Env.of_list eqs in
    let rec explore = function
      | [] -> []
      | var::q -> 
          if Env.mem var eqn_map then
            (var, Env.find var eqn_map)::explore q
          else
            explore q (* Cas d'une variable nulle *)
    in {
      p_eqs = explore toposort;
      p_inputs = p.p_inputs;
      p_outputs = p.p_outputs;
      p_vars = p.p_vars
    }
  ) with Cycle -> raise Combinational_cycle)
