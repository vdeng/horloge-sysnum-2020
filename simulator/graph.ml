exception Cycle
type mark = NotVisited | InProgress | Visited

module Smap = Map.Make(String)

type 'a graph =
    { mutable g_nodes : 'a node Smap.t }
and 'a node = {
  n_label : 'a;
  mutable n_mark : mark;
  mutable n_link_to : 'a node list;
  mutable n_linked_by : 'a node list;
}

let mk_graph () = { g_nodes = Smap.empty }

let add_node g x =
  let n = { n_label = x; n_mark = NotVisited; n_link_to = []; n_linked_by = [] } in
  g.g_nodes <- Smap.add x n g.g_nodes

let node_for_label g x =
  Smap.find x g.g_nodes

let add_edge g id1 id2 =
  let n1 = node_for_label g id1 in
  let n2 = node_for_label g id2 in
  n1.n_link_to <- n2::n1.n_link_to;
  n2.n_linked_by <- n1::n2.n_linked_by

let clear_marks g =
  Smap.iter (fun _ n -> n.n_mark <- NotVisited) g.g_nodes

let find_roots g =
  Smap.filter (fun _ n -> n.n_linked_by = []) g.g_nodes

let find_cycle g =  (*failwith "Graph.has_cycle: Non implementé"*)
  clear_marks g;
  let rec parcoursNd nd =
    if nd.n_mark = InProgress then (
      Some (Some nd, [nd])
    )
    else if nd.n_mark = Visited then
      None
    else (
      nd.n_mark <- InProgress;
      let rec parcoursVois = function
        | ndv::q -> begin
          match parcoursNd ndv with
          | None -> parcoursVois q
          | Some (loop, list) ->
            match loop with
            | Some n' when n'.n_label = nd.n_label -> Some (None, list)
            | None -> Some (None, list)
            | _ -> Some (loop, nd::list)
        end
        | [] -> None
      in
      let res = parcoursVois nd.n_link_to
      in nd.n_mark <- Visited;
      res
    )
  and parcoursListe = function
    | [] -> None
    | (_,nd)::q -> 
      if nd.n_mark = NotVisited then (
      match parcoursNd nd with
      | Some (_, li) -> Some li
      | None -> parcoursListe q)
      else parcoursListe q
  in parcoursListe (Smap.bindings g.g_nodes)

let topological g = (* failwith "Graph.topological: Non implementé" *)
  match find_cycle g with
  | Some li ->
    Printf.eprintf "A cycle was found, involving the following variables:\n";
    List.iter (fun nd -> Printf.eprintf "- %s\n" nd.n_label) li;
    raise Cycle
  | None ->
    clear_marks g;
    let rec dfs nd finliste =
      nd.n_mark <- Visited;
      let rec parcoursVois acc = function
        | ndv::q -> if ndv.n_mark = NotVisited then parcoursVois (dfs ndv acc) q else parcoursVois acc q
        | [] -> acc
      in nd.n_label::parcoursVois finliste nd.n_link_to
    and parcoursListe acc = function
      | [] -> acc
      | (_,nd)::q -> if nd.n_mark = NotVisited then parcoursListe (dfs nd acc) q else parcoursListe acc q
    in parcoursListe [] (Smap.bindings g.g_nodes)
  

