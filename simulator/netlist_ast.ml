type ident = string

(* Environment using ident as key *)
module Env = struct
  include Map.Make(struct
    type t = ident
    let compare = compare
  end)

  let of_list l =
    List.fold_left (fun env (x, ty) -> add x ty env) empty l
end

type ty = TBit | TBitArray of int
type value = VBit of bool | VBitArray of bool array

type binop = Or | Xor | And | Nand

(* argument of operators (variable or constant) *)
type arg =
    | Avar of ident (* x *)
    | Aconst of value (* constant *)

(* Expressions (see MiniJazz documentation for more info on the operators) *)
type exp =
    | Earg of arg (* a: argument *)
    | Ereg of ident (* REG x : register *)
    | Enot of arg (* NOT a *)
    | Ebinop of binop * arg * arg (* OP a1 a2 : boolean operator *)
    | Emux of arg * arg * arg (* MUX a1 a2 : multiplexer *)
    | Erom of int (*addr size*) * int (*word size*) * arg (*read_addr*)
        (* ROM addr_size word_size read_addr *)
    | Eram of int (*addr size*) * int (*word size*)
        * arg (*read_addr*) * arg (*write_enable*)
        * arg (*write_addr*) * arg (*data*)
        (* RAM addr_size word_size read_addr write_enable write_addr data *)
    | Econcat of arg * arg (* CONCAT a1 a2 : concatenation of arrays *)
    | Eslice of int * int * arg
      (* SLICE i1 i2 a : extract the slice of a between indices i1 and i2 *)
    | Eselect of int * arg
      (* SELECT i a : ith element of a *)

(* equations: x = exp *)
type equation = ident * exp

type program =
    { p_eqs : equation list; (* equations *)
      p_inputs : ident list; (* inputs *)
      p_outputs : ident list; (* outputs *)
      p_vars : ty Env.t; } (* maps variables to their types*)




type nident = int

type narg = 
    | NAvar of nident
    | NAconst of bool array

type nexp =
    | NEarg of narg (* a: argument *)
    | NEreg of nident (* REG x : register *)
    | NEnot of narg (* NOT a *)
    | NEbinop of binop * narg * narg (* OP a1 a2 : boolean operator *)
    | NEmux of narg * narg * narg (* MUX a1 a2 : multiplexer *)
    | NErom of int (*addr size*) * int (*word size*) * narg (*read_addr*)
        (* ROM addr_size word_size read_addr *)
    | NEram of int (*addr size*) * int (*word size*)
        * narg (*read_addr*) * narg (*write_enable*)
        * narg (*write_addr*) * narg (*data*)
        (* RAM addr_size word_size read_addr write_enable write_addr data *)
    | NEconcat of narg * narg (* CONCAT a1 a2 : concatenation of arrays *)
    | NEslice of int * int * narg
      (* SLICE i1 i2 a : extract the slice of a between indices i1 and i2 *)
    | NEselect of int * narg
      (* SELECT i a : ith element of a *)

type neqn = nident * nexp

type nprog =
    { np_eqs : neqn list;
      np_inputs: nident list;
      np_outputs: nident list;
      np_vars: (ident * ty) array (* Stocke pour chaque variable son identifiant original et son type *)}

let to_narg var_map = function
  | Avar x -> NAvar (Env.find x var_map)
  | Aconst (VBit b) -> NAconst [|b|]
  | Aconst (VBitArray a) -> NAconst a

let to_nexp var_map =
  let to_narg = to_narg var_map in
  function
  | Earg arg -> NEarg (to_narg arg) (* a: argument *)
  | Ereg id -> NEreg (Env.find id var_map) (* REG x : register *)
  | Enot arg -> NEnot (to_narg arg) (* NOT a *)
  | Ebinop (bin, a1, a2) -> NEbinop (bin, to_narg a1, to_narg a2) (* OP a1 a2 : boolean operator *)
  | Emux (c, a1, a2) -> NEmux (to_narg c, to_narg a1, to_narg a2) (* MUX a1 a2 : multiplexer *)
  | Erom (addr_sz, word_sz, read_ad) -> NErom (addr_sz, word_sz, to_narg read_ad)
      (* ROM addr_size word_size read_addr *)
  | Eram (addr_sz, word_sz, read_ad, write_en, write_ad, write_data) ->
    NEram (addr_sz, word_sz, to_narg read_ad, to_narg write_en, to_narg write_ad, to_narg write_data)
      (* RAM addr_size word_size read_addr write_enable write_addr data *)
  | Econcat (a1, a2) -> NEconcat (to_narg a1, to_narg a2) (* CONCAT a1 a2 : concatenation of arrays *)
  | Eslice (a, b, arg) -> NEslice (a, b, to_narg arg)
    (* SLICE i1 i2 a : extract the slice of a between indices i1 and i2 *)
  | Eselect (i, a) -> NEselect (i, to_narg a)
    (* SELECT i a : ith element of a *)

let to_neqn var_map (id, exp) = (Env.find id var_map, to_nexp var_map exp)
