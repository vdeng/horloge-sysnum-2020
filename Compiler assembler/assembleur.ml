open Ast
open Format

(*La compilation procède dans cet ordre : analyse lexicale, analyse syntaxique, repérage de toutes les étiquettes, changement des noms des étiquettes dans les BEQ, conversion en bits, écriture des bits dans un fichier bin*)



type option_compile = All | Print_only

(*Permet juste d'afficher les codes des caractères d'une liste de listes de char *)

let rec print_bin l = match l with
  |[] -> ()
  |t::q -> (match t with 
    |[] -> print_char '\n';print_bin q
    |t1::q1 -> print_int (int_of_char t1); print_char ';'; 
    print_bin (q1::q)
  )
(* Write_bin prend un nom de fichier et une liste de listes de caractères et écrit les caractères dans file.bin un par un*)
let write_bin file lc =
  let name = String.sub file 0 (String.length file - 4) in
  let newfile = String.concat "" [name;".bin"] in
  let oc = open_out newfile in
  let write c =
    let s = Bytes.make 1 c in
    output oc s 0 1
  in let rec aux l = match l with
    |[] -> ()
    |[]::q -> aux q
    |(t::q)::r -> write t; aux (q::r)
  in aux lc 


(*extension récupère l'extension à quatre caractères du fichier (point compris)*)
let extension file = 
  let l = String.length file in
  String.sub file (l-4) 4

(*programme principal. Prend le nom du fichier et une option parmi print et all et exécute les différentes parties de la compilation*)
let compile opt file = 
  (if extension file <> ".txt" then
    (print_string "File must have \".txt\" extension";
    exit 1));
  
let channel = try open_in file with _ -> print_string "Fichier inexistant"; exit 2 in
let lex = Lexing.from_channel channel in
let arbre = (try Parser.file Lexer.stoken lex with 
  |IllegalCharacter -> let c = Lexing.lexeme_start_p lex, Lexing.lexeme_end_p lex in failure file c "Caractère illégal"
  |Parser.Error -> let c = Lexing.lexeme_start_p lex, Lexing.lexeme_end_p lex in failure file c "Erreur de syntaxe"
)
in 
let l = try Labeller.labeller arbre with   |UndefinedLabel n -> let c = Lexing.lexeme_start_p lex, Lexing.lexeme_end_p lex in failure file c (sprintf "Le label %d n'a pas été défini" n)
  |AlreadyDefinedLabel n -> let c = Lexing.lexeme_start_p lex, Lexing.lexeme_end_p lex in failure file c (sprintf "Le label %d a déjà été défini" n)

in let lc = List.map Converter.ins_to_char_list l
in if opt = Print_only then print_bin lc else write_bin file lc

(*Les arg_trucs appellent compile avec la bonne option*)
let arg_print s = compile Print_only s

let arg_all s = compile All s

(*Main est celui qui récupère l'argument de l'opérateur et appelle l'un des arg_trucs*)
let main () = (*main va appeler suivant l'argument donné par l'opérateur une des fonctions arg_truc, qui appelera compile en précisant l'option. Puis compile s'arrêtera quand il le faut.*)
  Arg.parse ["--print-only", Arg.String arg_print, "Afficher la séquence binaire correspondante"] 
  (arg_all) "Crée un fichier binaire"

let () = main ()