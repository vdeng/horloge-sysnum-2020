open Ast

(*labels renvoie une table de hashing avec les positions des étiquettes*)

let labels s =
  let h = Hashtbl.create 17 in
  let line = ref Int64.zero in
  let rec aux l = match l with
    |[] -> ()
    |t::q -> (
      match t with
      |SLabel n -> if Hashtbl.mem h n then (raise (AlreadyDefinedLabel(n))) else Hashtbl.add h n !line; aux q
      |_ -> line := Int64.add !line 1L; aux q)
  in aux s; h

 (* labeller renvoie un arbre dans lequel les LABELS sont supprimés et les BEQ renvoient à des numéros d'instructions et non des étiquettes*)

let labeller s =
  let h = labels s in
  let aux1 i = 
    match i with
    |SLabel _ -> false
    |_ -> true
  in let ss = List.filter aux1 s in
  let aux2 i = match i with
  |STerins (t,v,r1,r2) -> TTerins (t,v,r1,r2)
  |SBinins (b,v,r1) -> TBinins (b,v,r1)
  |SBeq (n,r1,r2) -> let i = try Hashtbl.find h n with _ -> (raise (UndefinedLabel(n))) in TBeq (Vimm i, r1,r2)
  |SMovlabel (n,r) -> let i = try Hashtbl.find h n with _ -> (raise (UndefinedLabel(n))) in TTerins (Add, Vimm i, "r0", r)
  |SBeqr (r,r1,r2) -> TBeq (Vreg r, r1, r2)
  |_ -> assert false
  in List.map aux2 ss

  