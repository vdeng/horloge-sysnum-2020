open Ast

(*Converter est le fichier qui transforme un fichier en liste de liste de caractères.
La liste de liste de caractères a pour éléments des listes correspondant chacune à une instruction et ayant pour éléments 8 caractères ayant les codes binaires demandés.*)


let print_char_bits c =
  let intc = int_of_char c in
  let car i = if (intc lsr (7-i)) land 1 = 1 then '1' else '0' in
  Printf.printf "%s" (String.init 8 car)

let rec power64 i1 p = match p with
  |0 -> 1L
  |_ -> Int64.mul i1 (power64 i1 (p-1))

let int64_to_char_list i =
  (*Printf.printf "Conversion %s\n" (Int64.to_string i);*)
  let i256 = power64 2L 8 in
  (*Printf.printf "%s\n" (Int64.to_string i256);*)
  let b = ref i in
  let res = ref [] in
  let rec aux i = match i with
    |4 -> ()
    |_ ->
      let q,r = Int64.div !b i256, Int64.rem !b i256 in
      let c = char_of_int (Int64.to_int r) in
      res:= c::(!res); b:= q; aux (i+1);
  in aux 0; 
  (*Printf.printf "Résultat :";
  List.iter print_char_bits !res;
  Printf.printf "\n";*)
  !res

let register_to_int r = match r with 
  |"r0" -> 0
  |"r1" -> 1
  |"r2" -> 2
  |"r3" -> 3
  |"r4" -> 4
  |"r5" -> 5
  |"r6" -> 6
  |"r7" -> 7
  |"r8" -> 8
  |"r9" -> 9
  |"r10" -> 10
  |"r11" -> 11
  |"r12" -> 12
  |"r13" -> 13
  |"r14" -> 14
  |"r15" -> 15
  |_-> assert false

let value_to_char_list v = match v with 
  |Vimm i -> int64_to_char_list i
  |Vreg r -> int64_to_char_list (Int64.of_int (register_to_int r))

let triop_to_int t = match t with
  |Add -> 0
  |Sub -> 1
  |Div -> 16
  |Rem -> 17
  |Mul -> 18
  |Slt -> 10
  |Sgt -> 11
  |And -> 2
  |Or -> 3
  |Xor -> 4
  |Lsl -> 8
  |Lsr -> 9
let binop_to_int t = match t with
  |Load -> 20
  |Store -> 21

let ins_to_ints i = (*Renvoie (true si on utilise un imm) * code de la commande * liste_char correspondant à la value * code des deux registres              *)
  match i with
  |TTerins (t,Vimm v,r1,r2) -> true, triop_to_int t, value_to_char_list (Vimm v), register_to_int r1, register_to_int r2
  |TTerins (t,v, r1,r2) -> false, triop_to_int t ,value_to_char_list v, register_to_int r1, register_to_int r2
  |TBinins (b,Vimm v, r2) -> true, binop_to_int b, value_to_char_list (Vimm v), 0, register_to_int r2
  |TBinins (b, v, r2) -> false, binop_to_int b ,value_to_char_list v, 0, register_to_int r2
  |TBeq (Vimm v,r1,r2) -> true, 28, value_to_char_list (Vimm v), register_to_int r1, register_to_int r2
  |TBeq (v,r1,r2) -> false, 28, value_to_char_list v, register_to_int r1, register_to_int r2

let ins_to_char_list i =
  let (b,op,v,r1,r2) = ins_to_ints i in
  let c = char_of_int 0 in
  let bop = if b then 32 + op else op in
  let cop = char_of_int bop in
  let cr = char_of_int (r1 * 16 + r2) in
  let res = v@[cr;c;c;cop] in
  (*Printf.printf "Résultat instruction : ";
  List.iter print_char_bits res;
  Printf.printf "\n";*)
  res