{
    open Ast
    open Parser
    open Lexing
}
let blank = ' ' | '\t'
let registe = "r0" | "r1" | "r2" | "r3" | "r4" | "r5" | "r6" | "r7" | "r8" | "r9" | "r10" | "r11" | "r12" | "r13" | "r14" | "r15"
let trip = "ADD" | "SUB" | "DIV" | "REM" | "MUL" | "SLT" | "SGT" | "AND" | "OR" | "XOR" | "LSL" | "LSR"
let binp = "LOAD" | "STORE"
let immediate = ['0'-'9']+
let lbl = '$' ['0'-'9']+


rule stoken = parse
    |blank {stoken lexbuf}
    |'#' {comment lexbuf}
    |'\n' {Lexing.new_line lexbuf; NEWLINE}
    |trip as t {TRIP t}
    |binp as b {BINP b}
    |"JMP" {JMP}
    |"MOV" {MOV}
    |"NOT" {NOT}
    |"SEQZ" {SEQZ}
    |"BEQ" {BEQ}
    |"LABEL" {LABEL}
    |"MOVLABEL" {MOVLABEL}
    |lbl as l {LABELNUM (int_of_string (String.sub l 1 ((String.length l)- 1)))}
    |immediate as i {IMMEDIATE (Int64.of_string i)}
    |registe as r {REGISTER r}
    |_ {raise IllegalCharacter}
    |eof {EOF}

and comment = parse
    |eof {EOF}
    |'\n' {Lexing.new_line lexbuf; NEWLINE}
    |_ {comment lexbuf}