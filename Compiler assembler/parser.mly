%{
    open Ast
    
%}
%token BEQ EOF NEWLINE LABEL JMP MOV NOT SEQZ MOVLABEL
%token <int> LABELNUM 
%token <Int64.t> IMMEDIATE
%token <string> REGISTER TRIP BINP
%start <Ast.sfichier> file
%%
file:
    |EOF {[]}
    |NEWLINE; f = file {f}
    |i = instruction; NEWLINE; f = file {i::f}
    |i = instruction; EOF {[i]}
;
instruction:
    |LABEL; n = LABELNUM {SLabel n}
    |t = TRIP; i = IMMEDIATE; rs2 = REGISTER; rd = REGISTER {STerins (triop_of_string t, Vimm i, rs2, rd)}
    |t = TRIP; rs1 = REGISTER; rs2 = REGISTER; rd = REGISTER {STerins (triop_of_string t, Vreg rs1, rs2, rd)}
    |b = BINP; i = IMMEDIATE; rd = REGISTER {SBinins ((binop_of_string b, Vimm i, rd))}
    |b = BINP; rs = REGISTER; rd = REGISTER {SBinins ((binop_of_string b, Vreg rs, rd))}
    |BEQ; n = LABELNUM; rs1 = REGISTER; rs2 = REGISTER {SBeq (n,rs1,rs2)}
    |BEQ; rd = REGISTER; rs1 = REGISTER; rs2 = REGISTER {SBeqr (rd,rs1,rs2)}
    |JMP; n = LABELNUM {SBeq (n, "r0", "r0")}
    |JMP; r = REGISTER {SBeqr (r, "r0", "r0")}
    |MOV; rs = REGISTER; rd = REGISTER {STerins (Add, Vreg "r0", rs, rd)}
    |MOV; i = IMMEDIATE; rd = REGISTER {STerins (Add, Vimm i, "r0",rd)}
    |NOT; rs = REGISTER; rd = REGISTER {STerins (Xor, Vimm 4294967295L,rs,rd)}
    |SEQZ; rs = REGISTER; rd = REGISTER {STerins (Slt, Vimm 1L, rs, rd)}
    |MOVLABEL; n = LABELNUM; r = REGISTER {SMovlabel (n,r)}