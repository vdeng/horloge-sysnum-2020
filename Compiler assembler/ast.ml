open Lexing
open Format

(*Les typers s sont la réécriture du fichier avec encore les étiquettes*)

type sfichier = sinstruction list
and sinstruction = STerins of triop * value * sregister * sregister | SBinins of binop * value * sregister |SLabel of int |SBeq of int * sregister * sregister |SMovlabel of int*sregister |SBeqr of sregister*sregister*sregister
and triop = Add | Sub | Div | Rem | Mul | Slt | Sgt | And | Or | Xor | Lsl | Lsr
and binop = Load | Store
and value  = Vimm of Int64.t | Vreg of sregister
and sregister = string

let triop_of_string s = match s with
  |"ADD" -> Add
  |"SUB" -> Sub
  |"DIV" -> Div
  |"REM" -> Rem
  |"MUL" -> Mul
  |"SLT" -> Slt
  |"SGT" -> Sgt
  |"AND" -> And
  |"OR" -> Or
  |"XOR" -> Xor
  |"LSL" -> Lsl
  |"LSR" -> Lsr
  |_ -> assert false
let binop_of_string s = match s with
  |"LOAD" -> Load
  |"STORE" -> Store
  |_ -> assert false

(*Les types t sont débarrassés des étiquettes*)

type tfichier = tinstruction list
and tinstruction = TTerins of triop * value * sregister * sregister | TBinins of binop * value * sregister |TBeq of value * sregister * sregister

(*Partie traitement des erreurs*)

exception IllegalCharacter
exception AlreadyDefinedLabel of int
exception UndefinedLabel of int

let failure file (a,b) message = 
  let line = a.pos_lnum in
  let debut = a.pos_cnum - a.pos_bol in
  let longueur = b.pos_cnum - a.pos_cnum in
  let fmt = std_formatter in
  fprintf fmt "File \"%s\", line %d, characters %d-%d:\n%s\n" file line debut (debut + longueur) message;
  exit 1
