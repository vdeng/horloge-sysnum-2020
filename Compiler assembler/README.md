# Compilateur du langage assembleur pour le projet sysnum-2020 :

Le dossier contient huit fichiers indispensables : Makefile, dune et son dune-project, le fichier principal assembler ainsi que les fichiers ast, lexer, parser, labeller, converter. Il contient un fichier secondaire 'Exemple.txt' qui est un exemple typique de fichier accepté par le compilateur.

# Comment utiliser le compilateur ?

Il faut utiliser d'abord la commande 'make' qui créera un exécutable assembler. Celui-ci s'appelle avec la commande './assembler file.txt' et crée un fichier binaire 'file.bin'. 
Il est possible d'utiliser l'option './assembler --print-only file.txt' qui détecte toute erreur normalement mais au lieu de créer file.bin, il affiche les bits par ligne de 64 et par mots de 8. Cette option est plus utilisée pour détecter une erreur du compilateur.
A noter que le compilateur refuse tout fichier n'ayant pas son extension en '.txt'

# A quoi servent les fichiers ?

'Makefile' et les deux dune servent à la création de l'exécutable.
assembler récupère le fichier à analyser et appelle dans l'ordre les autres programmes, gère leurs exceptions et fait les transitions. Dans l'ordre :
- Lexer réalise l'analyse lexicale
- Parser réalise l'analyse syntaxique. Il conserve les Labels, mais il transforme toutes les pseudos-instructions.
- Labeller retire les étiquettes et ajoute les bonnes destinations aux BEQ.
- Converter crée le fichier binaire sous la forme d'une liste de liste de caractères (un caractère correspond à 8 bits et une liste de caractères à 64 bits, soit une instruction.)