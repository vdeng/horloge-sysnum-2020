MOV 32 r3 # 0
MOV 10 r4 # 1
MOVLABEL $1 r15 # 2
JMP $2 # 3
# Division euclidienne de r3=a par r4=b. a = q*b+r avec q dans r5 et r dans r3
LABEL $2
SLT r4 r3 r6 #r6 <- (r3 < r4) -- 4
MOV 1 r7 #r7 <- 1 -- 5
BEQ r15 r7 r6 #Jmp à r15 si r6 = r7 (= 1) -- 6
SUB r4 r3 r3 #r3 <- r3 - r4 -- 7
ADD 1 r5 r5 #r5 += 1 -- 8
JMP $2 # 9

LABEL $1
ADD r0 r3 r3 # 10
JMP $1 # 11