Projet de systèmes numériques 2020 : horloge
--------------------------------------------

Hector Buffière, Rémy Défossez, Victor Deng  
https://git.eleves.ens.fr/vdeng/horloge-sysnum-2020

## TL;DR

Pour compiler puis exécuter l'horloge, exécuter `make` puis `./clock.sh`.

# Présentation

Ce dépôt contient les sources et le rapport de notre projet de systèmes numériques 2020 : une horloge numérique s'exécutant sur un microprocesseur virtualisé. On y trouve :
- un simulateur de netlist (dossier `simulator/`), capable d'exécuter une netlist produite par Minijazz,
- Minijazz en tant que sous-module (dossier `minijazz`),
- un processeur en deux versions : version complète (`simulator/test/proc.mj`), version sans multiplication et division (`simulator/test/proc_simple.mj`),
- un assembleur ciblant ce processeur (dossier `Compiler assembler`),
- le code d'une horloge en assembleur pouvant s'exécuter sur ce processeur (dossier `Code horloge`),
- un rapport détaillé expliquant le fonctionnement des différentes composantes du projet (`rapport.pdf`).

# Compilation

## Prérequis

Pour compiler ce projet sous Linux, il faut :
- `make`
- une installation d'OCaml (4.08.1)
- `ocamlbuild` (0.14)
- `dune` (2.7.1)
- `menhir` (20200624)

Les versions entre parenthèses ont été testées. Toute version ultérieure devrait également fonctionner.

## Téléchargement des sources

Pour cloner ce dépôt, exécuter :

`git clone --recursive https://git.eleves.ens.fr/vdeng/horloge-sysnum-2020.git`

Si vous avez oublié l'option `--recursive`, depuis le dossier du dépôt, exécuter :

`git submodule update --init`

## Compilation

Exécuter `make` depuis le dossier du projet. Cela compilera Minijazz, l'assembleur, l'horloge, le simulateur et le processeur.

Pour effacer les sorties de la compilation, exécuter `make clean`.

Les cibles `make` suivantes sont utilisables pour compiler un composant en particulier : `assembler`, `clock`, `minijazz`, `processor`, `simulator`.  
Pour compiler tous les tests du processeur, exécuter `make` depuis `simulator/test/`.


# Utilisation

## Assembleur

La syntaxe de l'assembleur est décrite dans le rapport : en résumé, il y a une instruction par ligne non vide de la forme `OP rs2/imm rs1 rd` (les opérateurs sont décrits dans le rapport, section ISA) et une gestion de labels numériques (déclaration par `LABEL $xx` et accès par `MOVLABEL`, `JMP` ou `BEQ`). Le programme `assembleur` se trouve après compilation dans le dossier `Compiler assembler`.

Pour assembler un fichier source `<nom>.txt`, exécuter `./assembleur <nom>.txt` : le fichier binaire produit se trouve dans `<nom>.bin`. (Il est évidemment possible de remplacer `./assembleur` par son chemin, de même pour `<nom>.txt`.)

Le programme `assembleur` accepte une option de débogage `--print-only` qui, au lieu d'écrire le résultat dans un fichier, l'affiche avec des 0 et des 1.

## Minijazz

L'exécutable `mjc.byte` se trouve après compilation dans le dossier `minijazz`.

Pour compiler un fichier `<nom>.mj` en netlist, exécuter `./mjc.byte <nom>.mj` : la netlist produite se trouve dans `<nom>.net`.

## Simulateur

L'exécutable `netlist_simulator.native` se trouve après compilation dans le dossier `simulator`. Il s'utilise de la manière suivante :

`./netlist_simulator.native [options] <netlist.net>`

Les options principales sont :
- `-c` : mode horloge, active les emplacements spéciaux d'entrée-sortie (détails dans le rapport) et affiche les segments de l'horloge en *ASCII art* et ne met à jour l'affichage que lorsqu'un segment est mis à jour, ou tous les 100 cycles. Désactivé par défaut.
- `-ff` : en mode horloge, active le mode *fast-forward* (avance rapide). Hors mode horloge, option sans effet.
- `--idate <YYYY-mm-ddTHH:mm:ss>` : en mode horloge, permet de définir l'heure initiale. Exemple : pour le 16/01/2021 à 23:09:23, écrire `--idate 2021-01-16T23:09:23`.
- `-R <fichier.bin>` : charge le fichier `fichier.bin` dans l'unique ROM de la netlist donnée, lorsqu'il y en a une (ou dans la première dans l'ordre du tri topologique de la netlist s'il y a plusieurs ROM).
- `--ram-size <taille>` : permet de définir la taille physique de la RAM en nombre de mots lorsqu'il n'y en a qu'une seule (lorsqu'il y en a plusieurs, s'applique à la première RAM rencontrée après tri topologique). Toute lecture en dehors des adresses [0, taille-1] fournit 0. Si ce paramètre n'est pas fourni, chaque RAM est de taille 2^(taille de l'adresse de cette RAM).
- `--rom-size <taille>` : idem, mais pour une ROM. Ignoré si `-R` est utilisé.

Pour une liste complète, exécuter `./netlist_simiulator.native -help` depuis `simulator/`.

Lorsque le simulateur demande des entrées, vous pouvez les écrire en binaire (exemple : `00101010` pour 8 bits) ou en décimal préfixé d'un `d` (exemple : `d42`) : dans ce dernier cas, seuls les entiers entre -2^63 et 2^63-1 sont acceptés, et tout bit de poids trop fort est ignoré. Les sorties du simulateur sont affichées en binaire et en décimal.

## Processeur, horloge

Pour l'horloge, exécuter `./clock.sh` depuis le dossier du projet, après compilation. Ce script accepte toutes les options disponibles pour `netlist_simulator.native`, y compris `-help`, `-ff` et `--idate` (voir ci-dessus pour plus de détails sur ces options).

Pour tout autre programme, exécuter depuis le dossier du projet `./simulator/netlist_simulator.native --ram-size <taille en nombre de mots> -R <programme.bin> simulator/test/proc.net`. Cependant, les seules sorties affichées sont l'horloge (avec l'option `-c`), les registres lus et les résultats d'un calcul pour chaque cycle.


# Plus de détails

Pour des détails sur le fonctionnement interne du processeur et des autres outils ainsi que sur les choix réalisés, lire le rapport.

Pour des détails sur la structure du code source de chaque composant (sauf Minijazz), lire les README.md dans les dossiers correspondants.
